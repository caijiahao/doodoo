module.exports = class extends doodoo.Controller {
    async isUserAuth() {
        const Token = this.query.Token || this.get("Token");
        if (!Token) {
            this.status = 401;
            this.fail("授权失败", "User Unauthorized");
            return false;
        }

        try {
            const decoded = this.jwtVerify(Token);
            const user = await this.model("user")
                .query({ where: { id: decoded.id } })
                .fetch();
            if (!user) {
                this.status = 401;
                this.fail("用户被禁用", "User Unauthorized");
                return false;
            }
            this.hook.run("wxaAnalysisUser", user.wxa_id, user.id);
            return (this.state.user = user);
        } catch (err) {
            console.log(err);
            this.status = 401;
            this.fail("授权失败", "User Unauthorized");
            return false;
        }
    }

    async isWxaAuth() {
        // const referer = this.get("Referer");
        // const referers = referer.substr(26).split("/");
        // const WxaAppId = referers[0];

        const referer = this.query.Referer || this.get("Referer");
        const reg = new RegExp("https://servicewechat.com/(\\w*)/(\\w*)/");
        if (!referer.match(reg)) {
            this.status = 500;
            this.fail("Referer有误", "Referer Unknow");
            return false;
        }

        const WxaAppId = RegExp.$1;
        if (!WxaAppId) {
            this.status = 401;
            this.fail("授权失败", "Wxa Unauthorized");
            return false;
        }

        try {
            const wxa = await this.model("wxa")
                .query({
                    where: {
                        authorizer_appid: WxaAppId
                    }
                })
                .fetch();
            if (!wxa) {
                this.status = 401;
                this.fail("小程序不存在", "Wxa Unauthorized");
                return false;
            }
            if (!wxa.authorized) {
                this.status = 401;
                this.fail("小程序已取消授权", "Wxa Has Been Unauthorized");
                return false;
            }
            if (!wxa.status) {
                this.status = 401;
                this.fail("小程序被禁用", "Wxa Status Unauthorized");
                return false;
            }

            return (this.state.wxa = wxa);
        } catch (err) {
            this.status = 401;
            this.fail("授权失败", "Wxa Unauthorized");
            return false;
        }
    }

    async isShopAuth() {
        if (!this.state.wxa) {
            return;
        }

        const appId = this.state.wxa.app_id;
        const shop = await this.model("shop")
            .query({
                where: {
                    app_id: appId
                }
            })
            .fetch();
        if (!shop) {
            this.status = 401;
            this.fail("店铺不存在", "Shop Unauthorized");
            return false;
        }
        return (this.state.shop = shop);
    }

    async isShopStatusAuth() {
        if (!this.state.shop) {
            return;
        }
        if (!this.state.shop.status) {
            this.status = 200;
            this.fail("店铺已打洋", "Shop Status Unauthorized");
            return false;
        }
    }
};
