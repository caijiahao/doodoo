const axios = require("axios");
const moment = require("moment");
module.exports = class extends doodoo.Controller {
    /**
     * @api {get} /shop/public/public/notfiy  内容管理支付完成
     *
     * @apiDescription 客户登录授权
     * @apiGroup wappointment_api
     * @apiVersion 0.0.1
     *
     * @apiSuccess {number} order_id 预约订单orderid
     *
     * @apiSampleRequest /shop/public/public/notfiy
     */

    async notfiy() {
        const token = this.query.token;
        const order_id = this.jwtVerify(token);
        try {
            const order = await this.model("order")
                .query(qb => {
                    qb.where("orderid", order_id);
                })
                .fetch({ withRelated: ["detail.product"] });
            if (!order.pay_status) {
                const detail = order.detail;
                const product = [];
                const sku = [];
                if (detail.length) {
                    for (const i in detail) {
                        if (detail[i].product.id) {
                            const productSale =
                                Number(detail[i].product.sale) +
                                Number(detail[i].num);
                            const productObj = {
                                id: detail[i].product.id,
                                sale: productSale
                            };
                            product.push(productObj);
                        }
                        if (detail[i].sku_id) {
                            const productsku = await this.model("productsku")
                                .query(qb => {
                                    qb.where("id", detail[i].sku_id);
                                })
                                .fetch();
                            if (productsku.id) {
                                const skuSale =
                                    Number(productsku.sale) +
                                    Number(detail[i].num);
                                const skuObj = {
                                    id: productsku.id,
                                    sale: skuSale
                                };
                                sku.push(skuObj);
                            }
                        }
                    }
                    if (product.length) {
                        await doodoo.bookshelf.Collection.extend({
                            model: this.model("product")
                        })
                            .forge(product)
                            .invokeThen("save");
                    }
                    if (sku.length) {
                        await doodoo.bookshelf.Collection.extend({
                            model: this.model("productsku")
                        })
                            .forge(sku)
                            .invokeThen("save");
                    }
                }
            }
            const pay_time = moment().format("YYYY-MM-DD HH:mm:ss");
            await this.model("order")
                .forge({ id: order.id, pay_status: 1, pay_time: pay_time })
                .save();
            await this.model("trade")
                .forge({ id: order.trade_id, status: 1 })
                .save();
            this.hook.run("productAnalysisTrade", order.id);
        } catch (err) {
            this.status = 401;
            this.fail("无效的数据", "Invalid Verify");
            return false;
        }
    }

    /**
     * @api {get} /shop/public/public/delivery  快递
     *
     * @apiDescription 客户登录授权
     * @apiGroup wappointment_api
     * @apiVersion 0.0.1
     *
     * @apiSuccess {String} num  快递单号
     * @apiSuccess {String} code 快递公司代码
     *
     * @apiSampleRequest /shop/public/public/delivery
     */

    async delivery() {
        const { num, code } = this.query;
        const url = `https://www.kuaidi100.com/autonumber/auto?num=${num}`;
        // console.log(url);
        try {
            const _data = await axios.get(url);
            // console.log("222200");
            // console.log(_data);
            if (_data && _data.data) {
                const url_code = `http://www.kuaidi100.com/query?type=${code}&postid=${num}`;
                const res = await axios.get(url_code);
                const arr = res.data.data;
                // console.log("1", res);
                // let kuaidi_info = [];
                // for (let i in arr) {
                //     // console.log(arr[i]);
                //     let date = arr[i].time.slice(0, 10);
                //     let time = arr[i].time.slice(11);
                //     let d = date.split("-");
                //     let day = new Date(d[0], d[1], d[2]).getDay();
                //     let week = "";
                //     switch (day) {
                //         case 0:
                //             week = "周日";
                //             break;
                //         case 1:
                //             week = "周一";
                //             break;
                //         case 2:
                //             week = "周二";
                //             break;
                //         case 3:
                //             week = "周三";
                //             break;
                //         case 4:
                //             week = "周四";
                //             break;
                //         case 5:
                //             week = "周五";
                //             break;
                //         case 6:
                //             week = "周六";
                //             break;
                //     }
                //     // console.log("context", arr[i].context);
                //     // console.log("date", date);

                //     // console.log("time", time);
                //     // console.log("week", week);
                //     let obj = {
                //         context: arr[i].context,
                //         date: date,
                //         time: time,
                //         week: week
                //     };
                //     if (kuaidi_info[i - 1] && kuaidi_info[i - 1].week == week) {
                //         obj.isFirst = false;
                //     } else {
                //         obj.isFirst = true;
                //     }
                //     if (i == arr.length - 1) {
                //         obj.isLast = true;
                //     } else {
                //         obj.isLast = false;
                //     }
                //     // console.log(obj);
                //     kuaidi_info.push(obj);
                // }
                // console.log(kuaidi_info);
                this.success(arr);
            }
        } catch (err) {
            this.fail(err);
        }
    }
};
